const app = require('./app');
const http = require('http');

const port = process.env.port || 8080;
const onListening = () => {
    console.log(`Server Started @ localhost:${port}`);
}
const onError = (error) => {
    console.log(error.code);
}

app.set('port', port);
const server = http.createServer(app);

server.on('listening', onListening);
server.on('error', onError);
server.listen(port);