const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    
    next();
})

app.get('/', (req, res, next) => {
    res.redirect('homepage');
});

app.get('/homepage', (req, res) => {
    res.send('Hello world!');
});

app.get('/aboutus', (req, res) => {
    res.sendFile(path.join(__dirname + '/Resources/about.html'));
})

app.use((req, res) => {
    res.status(404).send('There was an error');
})

module.exports = app;