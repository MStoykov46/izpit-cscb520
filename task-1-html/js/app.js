var divElements = document.querySelectorAll('div');
divElements.forEach(e => e.addEventListener('click', (event) => {
    alert('With native js ' + event.currentTarget.getAttribute('id'));
}))

var divElementsWithjQuery = $('div').on('click', (event) => {
    alert('With jQuery ' + event.currentTarget.getAttribute('id'));
});

// target points to the actual div which is clicked
// currentTarget is the current div as the event bubbles from the bottom to the top


